package set

type Set[T comparable] map[T]struct{}

func NewSet[T comparable](elements ...T) Set[T] {
	m := make(Set[T], len(elements))
	for _, el := range elements {
		m[el] = struct{}{}
	}
	return m
}

func (s Set[T]) Len() int { return len(s) }
func (s Set[T]) Add(el T) { s[el] = struct{}{} }
func (s Set[T]) Contains(el T) bool {
	_, ok := s[el]
	return ok
}
