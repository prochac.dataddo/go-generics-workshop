package set

import (
	"github.com/matryer/is"
	"testing"
)

func TestSet(t *testing.T) {
	t.Run("string", func(t *testing.T) {
		is := is.New(t)
		set := NewSet[string]("a", "b", "c", "b")
		is.Equal(set.Len(), 3)
		is.True(set.Contains("a"))
		is.True(!set.Contains("d"))
		set.Add("d")
		is.True(set.Contains("d"))
	})
	t.Run("int", func(t *testing.T) {
		is := is.New(t)
		set := NewSet[int]([]int{1, 2, 3, 2}...)
		is.Equal(set.Len(), 3)
		is.True(set.Contains(1))
		is.True(!set.Contains(4))
		set.Add(4)
		is.True(set.Contains(4))
	})
}
