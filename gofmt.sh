#!/bin/sh

# All Go code has replaced interface{} for any
# https://github.com/golang/go/commit/2580d0e08d5e9f979b943758d3c49877fb2324cb
# https://cs.opensource.google/go/go/+/refs/tags/go1.18beta2:src/fmt/print.go;l=273
gofmt -w -r 'interface{} -> any' .
