package max

import (
	"github.com/matryer/is"
	"testing"
)

func TestMax(t *testing.T) {
	t.Run("int", func(t *testing.T) {
		max := Max[int](1, 2, 3)
		is.New(t).Equal(max, 3)
		max2 := Max[int8](1, 2, 3)
		is.New(t).Equal(max2, int8(3))
	})
	t.Run("float", func(t *testing.T) {
		max := Max[float64](0.1, 0.2, 0.3)
		is.New(t).Equal(max, 0.3)
		max2 := Max[float32](0.1, 0.2, 0.3)
		is.New(t).Equal(max2, float32(0.3))
	})
	t.Run("byte", func(t *testing.T) {
		m1 := Max[byte]('A', 'B', 'C')
		is.New(t).Equal(m1, byte('C'))
	})
	t.Run("string", func(t *testing.T) {
		m1 := Max[string]("A", "B", "C")
		is.New(t).Equal(m1, "C")
	})
	t.Run("MyString", func(t *testing.T) {
		type MyString string
		m1 := Max[MyString]("A", "B", "C")
		is.New(t).Equal(m1, MyString("C"))
	})
}
