package max

//func Max[T int | float32 | float64 | byte | string](numbers ...T) T {
//	var max T
//	for i, number := range numbers {
//		if number > max || i == 0 {
//			max = number
//		}
//	}
//	return max
//}

// Ordered is a constraint that permits any ordered type: any type
// that supports the operators < <= >= >.
// If future releases of Go add new ordered types,
// this constraint will be modified to include them.
//
// Copied from "golang.org/x/exp/constraints"

import (
	"golang.org/x/exp/constraints"
)

func Max[T constraints.Ordered](numbers ...T) T {
	var max T
	for i, number := range numbers {
		if number > max || i == 0 {
			max = number
		}
	}
	return max
}
