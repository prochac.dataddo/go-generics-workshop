# Go Generics Workshop

In this workshop you can practice generics proposal which is coming in Go 1.18.

## Install Go 1.18

### defer Uninstall

remove alias

```
$ unalias go
```

remove standard library

```
$ rm -r $(go1.18beta2 env GOROOT)
```

remove `go1.18beta2` binary

```
$ rm $(\go env GOBIN)/go1.18beta2
```

### Install

**Go 1.18** is not out yet. Though, **Beta2** is out, and real release comes in March.

Prerequisites
 - go
 - git

Following command will install `go1.18beta2` binary and download standard library

```
$ go install golang.org/dl/go1.18beta2@latest && go1.18beta2 download
```

check the installation

```
$ go1.18beta2 version
```

The binary is downloaded to `$GOBIN` (`go env GOBIN`). So make sure you have it in your `$PATH`.

For further usage, I recommend to set up an alias.

```
$ alias go=go1.18beta2
$ go version
```

### sources

- [https://go.dev/doc/tutorial/generics#installing_beta](https://go.dev/doc/tutorial/generics#installing_beta)
- [https://go.dev/doc/manage-install](https://go.dev/doc/manage-install)

### For GoLand users

Go to `Settings` (`Preferences` on MacOS)

- Linux/Windows: `[Ctrl]+[Alt]+[S]`
- MacOS: `[⌘]`?

`[Settings/Preferences]=>[Go]=>[GOROOT]`

and add your path to downloaded SDK (`$ go1.18beta2 env GOROOT`). It will set up an alias in IDE terminal for you.
