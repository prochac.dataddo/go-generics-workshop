package unsupported

import (
	"fmt"
	"time"
)

type InterfaceWithMethod interface {
	Method()
}

// ForbiddenUnion makes type union with interface with method.
type ForbiddenUnion interface {
	InterfaceWithMethod | any
}

// ThisIsOK and it's implemented by time.Duration.
type ThisIsOK interface {
	~int64
	fmt.Stringer // interface with String() method
}

// This is not ok, because interface contains type constraint. It can be used
// only as type parameter then.
var _ ThisIsOK = time.Duration(0)

// ForbiddenUnion2 uses comparable in type union. It's forbidden for some reason.
type ForbiddenUnion2 interface {
	comparable | any
}

// Constraint limits type to be int or float64.
type Constraint interface {
	int | float64
}

// BadConstraintUse uses type constraint wrongly.
type BadConstraintUse struct {
	IntOrFloat Constraint
}

// How would compiler know the type?
var _ = BadConstraintUse{IntOrFloat: 0}

// CorrectConstraintUse uses type constraint correctly.
type CorrectConstraintUse[T Constraint] struct {
	IntOrFloat T
}

// The type is known during compile time
var _ = CorrectConstraintUse[int]{IntOrFloat: 0}
var _ = CorrectConstraintUse[float64]{IntOrFloat: 0}

// permitted by Constraint
var _ = CorrectConstraintUse[complex128]{IntOrFloat: 0}

type MyInt int

func howToMakeTypeSwitch[T ~int | ~int32 | ~int64](i T) {
	// you can call *.(type) above interface only.
	switch any(i).(type) {
	case int:
		fmt.Println("int")
	case int32:
		fmt.Println("int32")
	case int64:
		fmt.Println("int64")
	case MyInt:
		fmt.Println("MyInt")
	}
}
