package maps

import (
	"github.com/matryer/is"
	"testing"
)

func TestClone(t *testing.T) {
	t.Run("string-string", func(t *testing.T) {
		m := map[string]string{
			"foo": "bar",
		}
		clone1 := Clone[map[string]string, string, string](m)
		is.New(t).Equal(m, clone1)
	})
	t.Run("string-MyString", func(t *testing.T) {
		type MyString string
		m := map[string]MyString{
			"foo": "bar",
		}
		clone1 := Clone[map[string]MyString, string, MyString](m)
		is.New(t).Equal(m, clone1)
	})
	t.Run("MyMap", func(t *testing.T) {
		type MyMap map[string]string
		m := MyMap{
			"foo": "bar",
		}
		clone1 := Clone[MyMap, string, string](m)
		is.New(t).Equal(m, clone1)
	})
	t.Run("MyMap/MyString", func(t *testing.T) {
		type MyString string
		type MyMap map[MyString]MyString
		m := MyMap{
			"foo": "bar",
		}
		clone1 := Clone[MyMap, MyString, MyString](m)
		is.New(t).Equal(m, clone1)
	})
}
