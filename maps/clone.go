package maps

// Clone returns a copy of m.  This is a shallow clone:
// the new keys and values are set using ordinary assignment.
//
// copy from https://cs.opensource.google/go/x/exp/+/6cf2b201:maps/maps.go;l=65
func Clone[M ~map[K]V, K comparable, V any](m M) M {
	r := make(M, len(m))
	for k, v := range m {
		r[k] = v
	}
	return r
}
