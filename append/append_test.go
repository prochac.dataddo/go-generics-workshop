package append

import (
	"github.com/matryer/is"
	"testing"
)

func TestAppend(t *testing.T) {
	t.Run("string", func(t *testing.T) {
		var sl []string
		sl = Append[string](sl, "foo", "bar")
		is.New(t).Equal(sl, []string{"foo", "bar"})
	})
	t.Run("uint64", func(t *testing.T) {
		var sl []uint64
		sl = Append[uint64](sl, 1, 2)
		is.New(t).Equal(sl, []uint64{1, 2})
	})
	t.Run("any", func(t *testing.T) {
		var sl []any
		sl = Append[any](sl, "foo", 1)
		is.New(t).Equal(sl, []any{"foo", 1})
	})
	t.Run("MySlice", func(t *testing.T) {
		type MySlice[T any] []T
		var sl MySlice[string]
		sl = Append[string](sl, "foo", "bar")
		is.New(t).Equal(sl, MySlice[string]{"foo", "bar"})
	})
}
