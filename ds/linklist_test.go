package ds

import (
	"github.com/matryer/is"
	"reflect"
	"testing"
)

func TestLinkedList_Append(t *testing.T) {
	tests := []struct {
		name   string
		values []int
		want   *LinkedList[int]
	}{
		{
			name:   "",
			values: []int{1, 2, 3, 4, 5},
			want:   NewLinkedList[int](1, 2, 3, 4, 5),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var l LinkedList[int]
			for _, value := range tt.values {
				l.Append(value)
			}
			is := is.New(t)
			is.Equal(&l, tt.want)
		})
	}
}

func TestLinkedList_AtIndex(t *testing.T) {
	tests := []struct {
		name  string
		l     *LinkedList[int]
		index int
		want  int
	}{
		{
			name:  "first",
			l:     NewLinkedList(1, 2, 3, 4, 5),
			index: 0,
			want:  1,
		},
		{
			name:  "second",
			l:     NewLinkedList(1, 2, 3, 4, 5),
			index: 1,
			want:  2,
		},
		{
			name:  "last",
			l:     NewLinkedList(1, 2, 3, 4, 5),
			index: 4,
			want:  5,
		},
		{
			name:  "out of index #1",
			l:     NewLinkedList(1, 2, 3, 4, 5),
			index: -1,
			want:  0,
		},
		{
			name:  "out of index #2",
			l:     NewLinkedList(1, 2, 3, 4, 5),
			index: 5,
			want:  0,
		},
		{
			name:  "empty list #1",
			l:     &LinkedList[int]{},
			index: 0,
			want:  0,
		},
		{
			name:  "empty list #2",
			l:     &LinkedList[int]{},
			index: 1,
			want:  0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _ := tt.l.AtIndex(tt.index)
			// TODO bool test check
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AtIndex() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLinkedList_ReplaceAt(t *testing.T) {
	tests := []struct {
		name     string
		l        *LinkedList[int]
		index    int
		value    int
		want     *LinkedList[int]
		wantFail bool
	}{
		{
			name:     "first",
			l:        NewLinkedList(1, 2, 3, 4, 5),
			index:    0,
			value:    42,
			want:     NewLinkedList(42, 2, 3, 4, 5),
			wantFail: false,
		},
		{
			name:     "last",
			l:        NewLinkedList(1, 2, 3, 4, 5),
			index:    4,
			value:    42,
			want:     NewLinkedList(1, 2, 3, 4, 42),
			wantFail: false,
		},
		{
			name:     "middle",
			l:        NewLinkedList(1, 2, 3, 4, 5),
			index:    2,
			value:    42,
			want:     NewLinkedList(1, 2, 42, 4, 5),
			wantFail: false,
		},
		{
			name:     "out of index #1",
			l:        NewLinkedList(1, 2, 3, 4, 5),
			index:    -1,
			value:    42,
			want:     nil,
			wantFail: true,
		},
		{
			name:     "out of index #2",
			l:        NewLinkedList(1, 2, 3, 4, 5),
			index:    5,
			value:    42,
			want:     nil,
			wantFail: true,
		},
		{
			name:     "empty list #1",
			l:        &LinkedList[int]{},
			index:    0,
			value:    42,
			want:     nil,
			wantFail: true,
		},
		{
			name:     "empty list #2",
			l:        &LinkedList[int]{},
			index:    1,
			value:    42,
			want:     nil,
			wantFail: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ok := tt.l.ReplaceAt(tt.index, tt.value)
			is := is.New(t)
			is.Equal(ok, !tt.wantFail)
			if tt.wantFail {
				return
			}
			is.Equal(tt.l, tt.want)
		})
	}
}

func TestLinkedList_Size(t *testing.T) {
	tests := []struct {
		name string
		l    *LinkedList[int]
		want int
	}{
		{
			name: "empty",
			l:    NewLinkedList[int](),
			want: 0,
		},
		{
			name: "one",
			l:    NewLinkedList(1),
			want: 1,
		},
		{
			name: "ten",
			l:    NewLinkedList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
			want: 10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.l.Size(); got != tt.want {
				t.Errorf("Size() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLinkedList_RemoveLast(t *testing.T) {
	tests := []struct {
		name        string
		l           *LinkedList[int]
		want        *LinkedList[int]
		wantRemoved int
		wantFail    bool
	}{
		{
			name:        "empty",
			l:           NewLinkedList[int](),
			want:        NewLinkedList[int](),
			wantRemoved: 0,
			wantFail:    true,
		},
		{
			name:        "one",
			l:           NewLinkedList(1),
			want:        NewLinkedList[int](),
			wantRemoved: 1,
			wantFail:    false,
		},
		{
			name:        "ten",
			l:           NewLinkedList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
			want:        NewLinkedList(1, 2, 3, 4, 5, 6, 7, 8, 9),
			wantRemoved: 10,
			wantFail:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			is := is.New(t)
			removed, ok := tt.l.RemoveLast()
			is.Equal(tt.l, tt.want)
			is.Equal(removed, tt.wantRemoved)
			is.Equal(ok, !tt.wantFail)
		})
	}
}

func TestLinkedList_ToSlice(t *testing.T) {
	tests := []struct {
		name string
		l    *LinkedList[int]
		want []int
	}{
		{
			name: "empty",
			l:    NewLinkedList[int](),
			want: []int{},
		},
		{
			name: "one",
			l:    NewLinkedList(1),
			want: []int{1},
		},
		{
			name: "ten",
			l:    NewLinkedList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
			want: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sl := tt.l.ToSlice()
			is.New(t).Equal(sl, tt.want)
		})
	}
}
