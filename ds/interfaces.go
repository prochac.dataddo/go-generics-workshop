package ds

// ILinkedList is Java style interface. Though, It serves only as a template to
// what you should implement. Can you forgive me?
type ILinkedList[T any] interface {
	First() (T, bool)
	Last() (T, bool)
	AtIndex(i int) (T, bool)
	Prepend(v T)
	Append(v T)
	AddAt(i int, v T) bool
	ReplaceAt(i int, v T) bool
	RemoveFirst() (T, bool)
	RemoveLast() (T, bool)
	RemoveAt(i int) (T, bool)
	Clear()
	Size() int
	Iterator() Iterator[T]
	ToSlice() []T
}

// IDoubleLinkedList allows iteration from both sides. Also, it is more efficient
// for tail operations. ex. Last() or RemoveLast()
type IDoubleLinkedList[T any] interface {
	ILinkedList[T]
	ReverseIterator() Iterator[T]
}

// IExtendedLinkedList allows list content selecting and modifying also by its
// value, not just by the index.
type IExtendedLinkedList[T comparable] interface {
	ILinkedList[T]
	Remove(v T)
	Contains(v T)
	IndexOf(v T)
}

// Iterator provides simple mechanism for its content iteration.
type Iterator[T any] interface {
	// HasNext returns true if Next() can return next value.
	HasNext() bool
	// Next returns next T value. If there is no next value, the behaviour is
	// undefined.
	Next() T
}

// ResettableIterator is Iterator, which can be reset.
type ResettableIterator[T any] interface {
	Iterator[T]
	// Reset allows to start iteration from the beginning.
	Reset()
}
