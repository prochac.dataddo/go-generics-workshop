package ds

type element[T any] struct {
	value T
	next  *element[T]
}

type LinkedList[T any] struct {
	head *element[T]
}

var _ ILinkedList[int] = (*LinkedList[int])(nil)

func NewLinkedList[T any](elements ...T) *LinkedList[T] {
	var l LinkedList[T]
	for i := len(elements) - 1; i >= 0; i-- {
		el := elements[i]
		l.Prepend(el)
	}
	return &l
}

func (l *LinkedList[T]) First() (T, bool) {
	el, ok := l.first()
	if !ok {
		return l.zeroT(), false
	}
	return el.value, true
}

func (l *LinkedList[T]) Last() (T, bool) {
	v, ok := l.last()
	if !ok {
		return l.zeroT(), false
	}
	return v.value, true
}

func (l *LinkedList[T]) AtIndex(i int) (T, bool) {
	el, ok := l.atIndex(i)
	if !ok {
		return l.zeroT(), false
	}
	return el.value, true
}

func (l *LinkedList[T]) Prepend(v T) {
	el := &element[T]{
		next:  l.head,
		value: v,
	}
	l.head = el
}

func (l *LinkedList[T]) Append(v T) {
	el, ok := l.last()
	if !ok {
		l.head = &element[T]{value: v}
		return
	}
	el.next = &element[T]{value: v}
}

func (l *LinkedList[T]) AddAt(index int, v T) bool {
	if index < 0 {
		return false
	}
	if index == 0 {
		l.head = &element[T]{value: v, next: l.head}
		return true
	}
	el, ok := l.atIndex(index - 1)
	if !ok {
		return false
	}
	el.next = &element[T]{value: v, next: el.next}
	return true
}

func (l *LinkedList[T]) ReplaceAt(index int, v T) bool {
	if index < 0 {
		return false
	}
	if index == 0 {
		if l.head == nil {
			return false
		}
		l.head = &element[T]{value: v, next: l.head.next}
		return true
	}
	el, ok := l.atIndex(index - 1)
	if !ok {
		return false
	}
	if el.next == nil {
		return false
	}
	el.next = &element[T]{value: v, next: el.next.next}
	return true
}

func (l *LinkedList[T]) RemoveFirst() (T, bool) {
	el, ok := l.first()
	if !ok {
		return l.zeroT(), false
	}
	l.head = el.next
	return el.value, true
}

func (l *LinkedList[T]) RemoveLast() (T, bool) {
	s := l.Size()
	if s == 0 {
		return l.zeroT(), false
	}
	if s == 1 {
		return l.RemoveFirst()
	}
	el, ok := l.atIndex(s - 2)
	if !ok {
		return l.zeroT(), false
	}
	v := el.next.value
	el.next = nil
	return v, true
}

func (l *LinkedList[T]) RemoveAt(index int) (T, bool) {
	if index == 0 {
		return l.RemoveFirst()
	}
	el, ok := l.atIndex(index - 1)
	if !ok {
		return l.zeroT(), false
	}
	v := el.next.value
	el.next = nil
	return v, true
}

func (l *LinkedList[T]) Clear() {
	l.head = nil
}

func (l *LinkedList[T]) Size() int {
	if l.head == nil {
		return 0
	}
	var i int
	for el := l.head; el.next != nil; i++ {
		el = el.next
	}
	return i + 1
}

func (l *LinkedList[T]) ToSlice() []T {
	iter := l.Iterator()
	sl := make([]T, l.Size())
	for i := 0; iter.HasNext(); i++ {
		sl[i] = iter.Next()
	}
	return sl
}

func (l *LinkedList[T]) Iterator() Iterator[T] {
	return &iterator[T]{
		head: l.head,
		next: l.head,
	}
}

func (l *LinkedList[T]) first() (*element[T], bool) {
	if l.head == nil {
		return nil, false
	}
	return l.head, true
}

func (l *LinkedList[T]) last() (*element[T], bool) {
	if l.head == nil {
		return nil, false
	}
	el := l.head
	for el.next != nil {
		el = el.next
	}
	return el, true
}

func (l *LinkedList[T]) atIndex(i int) (*element[T], bool) {
	if i < 0 {
		return nil, false
	}
	if l.head == nil {
		return nil, false
	}
	el := l.head
	for j := 0; j < i; j++ {
		if el.next == nil {
			return nil, false
		}
		el = el.next
	}
	return el, true
}

func (l *LinkedList[T]) zeroT() (t T) { return }

type iterator[T any] struct {
	head *element[T]
	next *element[T]
}

var _ Iterator[int] = (*iterator[int])(nil)

func (i *iterator[T]) HasNext() bool {
	return i.next != nil
}

func (i *iterator[T]) Next() T {
	current := i.next
	i.next = current.next
	return current.value
}

func (i *iterator[T]) Reset() {
	i.next = i.head
}
