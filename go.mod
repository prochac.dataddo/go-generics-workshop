module gitlab.com/prochac.dataddo/go-generics-workshop

go 1.18

require (
	github.com/matryer/is v1.4.0
	golang.org/x/exp v0.0.0-20220218215828-6cf2b201936e
)
